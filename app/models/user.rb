class User < ActiveRecord::Base

  has_many :matches
  has_many :boost_mmrs
  has_many :conversations, :foreign_key => :sender_id

  class << self
    def from_omniauth(auth)
      info = auth['info']
      # Convert from 64-bit to 32-bit
      user = find_or_initialize_by(uid: (auth['uid'].to_i - 76561197960265728).to_s)
      user.nickname = info['nickname']
      user.avatar_url = info['image']
      user.profile_url = info['urls']['Profile']
      user.save!
      user
    end
  end

  def load_matches!

    matches_arr = matches_to_arr(self.uid)

    if matches_arr
      matches_arr.each do |match|
        unless self.matches.where(uid: match).any?
          match_info = Dota.api.matches(match)
=begin
          if match_info.starts_at >= Time.now
=end
          new_match = self.matches.create({
                                              uid: match,
                                              winner: match_info.winner.to_s.titleize,
                                              started_at: match_info.starts_at,
                                              mode: match_info.mode,
                                              duration: parse_duration(match_info.duration),
                                              match_type: match_info.type,
                                          })
        end
      end
    end
  end

  def won?(match)
    player = find_self_in(match)
    (player.radiant? && match.winner == 'Radiant') || (!player.radiant? && match.winner == 'Dire')
  end

  def played_for_in(match)
    find_self_in(match).hero
  end

  def player_items(match)
    find_self_in(match).items
  end

  private

  def find_self_in(match)
    match.players.find_by(uid: uid)
  end

  def matches_to_arr(uid)
    arr = []


    json = NetHttpService.new("https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?account_id=#{self.uid}&matches_requested=25&key=#{ENV["STEAM_KEY"]}").get_request

    JSON.parse(json)["result"]["matches"].each do |h|
      arr << h["match_id"]
    end

    arr
  end

  def parse_duration(d)
    hr = (d / 3600).floor
    min = ((d - (hr * 3600)) / 60).floor
    sec = (d - (hr * 3600) - (min * 60)).floor

    hr = '0' + hr.to_s if hr.to_i < 10
    min = '0' + min.to_s if min.to_i < 10
    sec = '0' + sec.to_s if sec.to_i < 10

    hr.to_s + ':' + min.to_s + ':' + sec.to_s
  end

end

=begin
require 'net/http'
=end

class Match < ActiveRecord::Base
  belongs_to :user
  has_many :players, dependent: :delete_all
  has_many :items
  after_save :load_players!

  def load_players!
    match_request =
        NetHttpService.new("https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?match_id=#{self.uid}&key=#{ENV["STEAM_KEY"]}").get_request
    match_info = JSON.parse(match_request)
=begin

    match_info = Dota.api.matches((self.uid).to_i)
    roster = {radiant: match_info.radiant, dire: match_info.dire}
    roster.each_pair do |k, v|
=end

    match_info["result"]["players"].each do |player|
      self.players.create({
                              uid: player["account_id"],
                              items: get_player_items(match_info, player["player_slot"]).map{ |item|  {
                                  id: item,
                                  name: name = item_name(item),
                                  image: "http://cdn.dota2.com/apps/dota2/images/items/#{name}_lg.png"
                              }},
                              hero: {id: player["hero_id"],
                                     name: name = hero_name(player["hero_id"]),
                                     localized_name: hero_localized_name(player["hero_id"]),
                                     image: "http://cdn.dota2.com/apps/dota2/images/heroes/#{name}_hphover.png"},
                              level: player["level"],
                              kills: player["kills"],
                              deaths: player["deaths"],
                              assists: player["assists"],
                              last_hits: player["last_hits"],
                              denies: player["denies"],
                              gold: player["gold"],
                              gpm: player["gold_per_min"],
                              xpm: player["xp_per_min"],
                              status: match_info["result"]["radiant_win"].to_s.titleize,
                              gold_spent: player["gold_spent"],
                              hero_damage: player["hero_damage"],
                              tower_damage: player["tower_damage"],
                              hero_healing: player["hero_healing"],
                              slot: player["player_slot"],
                              radiant: player["player_slot"] <= 4 ? true : false
                          })
    end
=begin
    end
=end
  end

  private

  def hero_name(id)
    Hero.find_by(dota_api_id: id).name
  end

  def hero_localized_name(id)
    Hero.find_by(dota_api_id: id).localized_name
  end

  def item_name(id)
    if id == 0
      "Empty"
    else
      Item.find_by(dota_api_id: id).name.gsub("kaya", "trident")
    end
    rescue => e
  end

  def get_player_items(match_hash, player_slot)
    player_params = match_hash["result"]["players"].find { |current| current["player_slot"] == player_slot}
    [player_params["item_0"], player_params["item_1"], player_params["item_2"],
     player_params["item_3"], player_params["item_4"], player_params["item_5"]]
  end

end

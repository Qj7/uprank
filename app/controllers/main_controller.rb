class MainController < ApplicationController
  def mmr_boost
    @heroes = Hero.all

    arr_of_str = ["Abaddon", "Alchemist", "Axe", "Beastmaster", "Brewmaster", "Bristleback", "Centaur Warrunner", "Chaos Knight", "Clockwerk", "Doom", "Dragon Knight", "Earth Spirit", "Earthshaker", "Elder Titan", "Huskar", "Io", "Kunkka", "Legion Commander", "Lifestealer", "Lycan", "Magnus", "Night Stalker", "Omniknight", "Phoenix", "Pudge", "Sand King", "Slardar", "Spirit Breaker", "Sven", "Tidehunter", "Timbersaw", "Tiny", "Treant Protector", "Tusk", "Underlord", "Undying", "Wraith King"]
    arr_of_agi = ["Anti-Mage", "Arc Warden", "Bloodseeker", "Bounty Hunter", "Broodmother", "Clinkz", "Drow Ranger", "Ember Spirit", "Faceless Void", "Gyrocopter", "Juggernaut", "Lone Druid", "Luna", "Medusa", "Meepo", "Mirana", "Monkey King", "Morphling", "Naga Siren", "Nyx Assassin", "Pangolier", "Phantom Assassin", "Phantom Lancer", "Razor", "Riki", "Shadow Fiend", "Slark", "Sniper", "Spectre", "Templar Assassin", "Terrorblade", "Troll Warlord", "Ursa", "Vengeful Spirit", "Venomancer", "Viper", "Weaver"]
    arr_of_int = ["Ancient Apparition", "Bane", "Batrider", "Chen", "Crystal Maiden", "Dark Seer", "Dark Willow", "Dazzle", "Death Prophet", "Disruptor", "Enchantress", "Enigma", "Invoker", "Jakiro", "Keeper of the Light", "Leshrac", "Lich", "Lina", "Lion", "Nature's Prophet", "Necrophos", "Ogre Magi", "Oracle", "Outworld Devourer", "Puck", "Pugna", "Queen of Pain", "Rubick", "Shadow Demon", "Shadow Shaman", "Silencer", "Skywrath Mage", "Storm Spirit", "Techies", "Tinker", "Visage", "Warlock", "Windranger", "Winter Wyvern", "Witch Doctor", "Zeus"]
    @arr_of_str = []
    @arr_of_agi = []
    @arr_of_int = []

    @heroes.each do |str_hero|
      arr_of_str.each do |hero|
        @arr_of_str << str_hero if str_hero.localized_name == hero
      end
    end

    @heroes.each do |str_hero|
      arr_of_agi.each do |hero|
        @arr_of_agi << str_hero if str_hero.localized_name == hero
      end
    end

    @heroes.each do |str_hero|
      arr_of_int.each do |hero|
        @arr_of_int << str_hero if str_hero.localized_name == hero
      end
    end

  end

  def preparation

  end

  def checkout

  end


end
class MatchesController < ApplicationController
  def index
    current_user.load_matches!
    @matches = current_user.matches.order('started_at DESC') if current_user
    @users = User.all

  end

  def show
    @match = Match.includes(:players).find_by(id: params[:id])
    @players = @match.players.order('slot ASC').group_by(&:radiant)
  end

end
class BoostMmrController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    days_of_week
=begin
    boost = BoostMmr.create(boostmmr_params)
=end
    redirect_to preparation_path(boostmmr_params)
  end

  def update

    BoostMmr.create boostmmr_params

    redirect_to checkout_path
  end

  private

  def days_of_week
=begin
    params[:boost_mmr][:schedule_days] = params[:boost_mmr][:schedule_days].delete_if{ |x| x == "" }.join(", ")
=end
    params[:schedule_days] = [:schedule_days].delete_if{ |x| x == "" }.join(", ")
  end

  def boostmmr_params
=begin
    params.permit(:current)
=end
    params.require(:boost).permit(:current, :desired, :stream, :party, :schedule, :schedule_days, :schedule_time,
                                  :schedule_body, :certain_heroes, :selected_heroes, :login, :password, :email, :user_id)
  end

end
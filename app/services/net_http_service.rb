class NetHttpService

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def get_request
    uri = URI(params)
    res = Net::HTTP.get_response(uri)
    res.body if res.is_a?(Net::HTTPSuccess)
  end
end
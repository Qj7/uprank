class CreateHeros < ActiveRecord::Migration
  def change
    create_table :heros do |t|
      t.integer :dota_api_id
      t.string :name
      t.string :localized_name
      t.timestamps null: false
    end
  end
end

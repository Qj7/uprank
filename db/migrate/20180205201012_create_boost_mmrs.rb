class CreateBoostMmrs < ActiveRecord::Migration
  def change
    create_table :boost_mmrs do |t|
      t.integer :current
      t.integer :desired
      t.string :stream
      t.string :party
      t.string :schedule
      t.string :schedule_days
      t.string :schedule_time
      t.string :schedule_body
      t.string :certain_heroes
      t.string :login
      t.string :password
      t.string :email
      t.text :selected_heroes
      t.references :user, index: true
      t.timestamps null: false
    end
  end
end

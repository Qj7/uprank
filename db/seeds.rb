# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

  json = NetHttpService.new("https://api.steampowered.com/IEconDOTA2_570/GetGameItems/V001/?&key=#{ENV["STEAM_KEY"]}").get_request
  items_hash = JSON.parse(json)
  items_hash["result"]["items"].each do |item|
  Item.create({
                          dota_api_id: item["id"],
                          name: item["name"].gsub("item_", "")
                      })
  end
  json2 = NetHttpService.new("https://api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?&key=#{ENV["STEAM_KEY"]}&language=en_us&format=JSON").get_request
  heroes_hash = JSON.parse(json2)
  heroes_hash["result"]["heroes"].each do |hero|
    Hero.create({
                    dota_api_id: hero["id"],
                    localized_name: hero["localized_name"],
                    name: hero["name"].gsub("npc_dota_hero_", "")
                })
  end
